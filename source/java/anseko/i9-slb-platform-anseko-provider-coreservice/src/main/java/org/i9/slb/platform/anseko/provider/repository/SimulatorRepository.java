package org.i9.slb.platform.anseko.provider.repository;

import org.i9.slb.platform.anseko.provider.dto.SimulatorDto;
import org.i9.slb.platform.anseko.provider.repository.mapper.SimulatorRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 模拟器dao
 *
 * @author R12
 * @date 2018年9月4日 10:49:40
 */
@Repository
public class SimulatorRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    /**
     * 获取模拟器列表
     *
     * @return
     */
    public List<SimulatorDto> getSimulatorDtoList() {
        String sql = "SELECT id, simulatorName, createDate, cpunum, ramnum, powerStatus, vncpassword, vncport, androidVersion, instance FROM t_simulator ORDER BY createDate DESC";
        List<SimulatorDto> simulatorDtos = this.jdbcTemplate.query(sql, new SimulatorRowMapper());
        return simulatorDtos;
    }

    /**
     * 获取模拟器信息
     *
     * @param simulatorId
     * @return
     */
    public SimulatorDto getSimulatorDtoById(String simulatorId) {
        String sql = "SELECT id, simulatorName, createDate, cpunum, ramnum, powerStatus, vncpassword, vncport, androidVersion, instance FROM t_simulator WHERE id = ?";
        List<SimulatorDto> simulatorDtos = this.jdbcTemplate.query(sql, new Object[]{simulatorId}, new SimulatorRowMapper());
        if (simulatorDtos.isEmpty()) {
            return null;
        }
        return simulatorDtos.get(0);
    }

    /**
     * 保存模拟器信息
     *
     * @param simulatorDto
     */
    public void insertSimulator(SimulatorDto simulatorDto) {
        String sql = "INSERT INTO t_simulator (id, simulatorName, createDate, cpunum, ramnum, powerStatus, vncpassword, vncport, androidVersion, instance) VALUES (?, ?, NOW(), ?, ?, ?, ?, ?, ?, ?)";
        this.jdbcTemplate.update(sql,
                new Object[]{
                        simulatorDto.getId(),
                        simulatorDto.getSimulatorName(),
                        simulatorDto.getCpunum(),
                        simulatorDto.getRamnum(),
                        simulatorDto.getPowerStatus(),
                        simulatorDto.getVncpassword(),
                        simulatorDto.getVncport(),
                        simulatorDto.getAndroidVersion(),
                        simulatorDto.getInstance()
                }
        );
    }

    /**
     * 获取当前实例中最大vncport
     *
     * @param instance
     * @return
     */
    public int getSimulatorMaxVNCPort(String instance) {
        String sql = "SELECT MAX(vncport) FROM t_simulator WHERE instance = ?";
        Integer vncport = this.jdbcTemplate.queryForObject(sql, new Object[]{instance}, Integer.class);
        return vncport == null ? 0 : vncport.intValue();
    }

    /**
     * 删除模拟器
     *
     * @param simulatorId
     */
    public void deleteSimulator(String simulatorId) {
        String sql = "DELETE FROM t_simulator WHERE id = ?";
        this.jdbcTemplate.update(sql, new Object[]{simulatorId});
    }

    /**
     * 更新模拟器状态
     *
     * @param simulatorId
     * @param powerState
     */
    public void updateSimulatorPowerState(String simulatorId, int powerState) {
        String sql = "UPDATE t_simulator SET powerStatus = ? WHERE id = ?";
        this.jdbcTemplate.update(sql, new Object[]{powerState, simulatorId});
    }
}
