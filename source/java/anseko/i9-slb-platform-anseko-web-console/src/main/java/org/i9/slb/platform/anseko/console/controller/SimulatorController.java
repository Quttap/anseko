package org.i9.slb.platform.anseko.console.controller;

import org.i9.slb.platform.anseko.common.PowerState;
import org.i9.slb.platform.anseko.console.service.SimulatorService;
import org.i9.slb.platform.anseko.console.service.impl.SimulatorServiceImpl;
import org.i9.slb.platform.anseko.console.utils.HttpResponse;
import org.i9.slb.platform.anseko.console.utils.Result;
import org.i9.slb.platform.anseko.provider.IDubboInstanceRemoteService;
import org.i9.slb.platform.anseko.provider.IDubboSimulatorRemoteService;
import org.i9.slb.platform.anseko.provider.dto.InstanceDto;
import org.i9.slb.platform.anseko.provider.dto.SimulatorDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;

/**
 * 模拟器控制器
 *
 * @author R12
 * @date 2018.08.30
 */
@Controller
public class SimulatorController {

    @Autowired
    private IDubboSimulatorRemoteService dubboSimulatorRemoteService;

    @Autowired
    private IDubboInstanceRemoteService dubboInstanceRemoteService;

    @Autowired
    private SimulatorService simulatorService;

    private static final Logger LOGGER = LoggerFactory.getLogger(SimulatorController.class);

    /**
     * 模拟器列表
     *
     * @return
     */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index(Model model) {
        HashMap<String, InstanceDto> result = new HashMap<String, InstanceDto>();
        List<InstanceDto> instanceDtos = this.dubboInstanceRemoteService.getInstanceDtoList();
        for (InstanceDto instanceDto : instanceDtos) {
            result.put(instanceDto.getId(), instanceDto);
        }
        List<SimulatorDto> simulatorDtos = this.dubboSimulatorRemoteService.getSimulatorDtoList();
        for (SimulatorDto simulatorDto : simulatorDtos) {
            InstanceDto instanceDto = result.get(simulatorDto.getInstance());
            simulatorDto.setInstanceDto(instanceDto);
        }
        model.addAttribute("simulatorDtos", simulatorDtos);
        model.addAttribute("instanceDtos", instanceDtos);
        return "index";
    }

    /**
     * vncviewer
     *
     * @param simulatorId
     * @param model
     * @return
     */
    @RequestMapping(value = "/vncviewer/{simulatorId}", method = RequestMethod.GET)
    public String vncviewer(@PathVariable("simulatorId") String simulatorId, Model model) {
        SimulatorDto simulatorDto = dubboSimulatorRemoteService.getSimulatorDtoInfo(simulatorId);
        model.addAttribute("simulatorDto", simulatorDto);
        return "vncviewer";
    }

    /**
     * 启动模拟器
     *
     * @param simulatorId
     * @throws Exception
     */
    @RequestMapping(value = "/start/{simulatorId}", method = RequestMethod.POST)
    @ResponseBody
    public Result start(@PathVariable("simulatorId") String simulatorId) {
        SimulatorDto simulatorDto = this.dubboSimulatorRemoteService.getSimulatorDtoInfo(simulatorId);
        this.dubboSimulatorRemoteService.refreshSimulatorPowerState(simulatorDto.getId(), PowerState.RUNING.ordinal());
        try {
            this.simulatorService.startSimulator(simulatorDto);
            LOGGER.info("启动模拟器 : {}, 成功", simulatorDto.getSimulatorName());
            return HttpResponse.ok();
        } catch (Exception e) {
            LOGGER.error("启动模拟器 : {}, 失败", simulatorDto.getSimulatorName(), e);
            return HttpResponse.error(e);
        }
    }

    /**
     * 启动模拟器
     *
     * @throws Exception
     */
    @RequestMapping(value = "/batch_start", method = RequestMethod.POST)
    @ResponseBody
    public Result start() {
        for (SimulatorDto simulatorDto : this.dubboSimulatorRemoteService.getSimulatorDtoList()) {
            this.dubboSimulatorRemoteService.refreshSimulatorPowerState(simulatorDto.getId(), PowerState.RUNING.ordinal());
            try {
                this.simulatorService.startSimulator(simulatorDto);
                LOGGER.info("批量启动模拟器 : {}, 成功", simulatorDto.getSimulatorName());
            } catch (Exception e) {
                LOGGER.error("批量启动模拟器 : {}, 失败", simulatorDto.getSimulatorName(), e);
            }
        }
        return HttpResponse.ok();
    }

    /**
     * 重启模拟器
     *
     * @param simulatorId
     * @throws Exception
     */
    @RequestMapping(value = "/reboot/{simulatorId}", method = RequestMethod.POST)
    @ResponseBody
    public Result reboot(@PathVariable("simulatorId") String simulatorId) {
        SimulatorDto simulatorDto = this.dubboSimulatorRemoteService.getSimulatorDtoInfo(simulatorId);
        this.dubboSimulatorRemoteService.refreshSimulatorPowerState(simulatorDto.getId(), PowerState.RUNING.ordinal());
        try {
            this.simulatorService.rebootSimulator(simulatorDto);
            LOGGER.info("重启模拟器 : {}, 成功", simulatorDto.getSimulatorName());
            return HttpResponse.ok();
        } catch (Exception e) {
            LOGGER.error("重启模拟器 : {}, 失败", simulatorDto.getSimulatorName(), e);
            return HttpResponse.error(e);
        }
    }

    /**
     * 重启模拟器
     *
     * @throws Exception
     */
    @RequestMapping(value = "/batch_reboot", method = RequestMethod.POST)
    @ResponseBody
    public Result reboot() {
        for (SimulatorDto simulatorDto : this.dubboSimulatorRemoteService.getSimulatorDtoList()) {
            this.dubboSimulatorRemoteService.refreshSimulatorPowerState(simulatorDto.getId(), PowerState.RUNING.ordinal());
            try {
                this.simulatorService.rebootSimulator(simulatorDto);
                LOGGER.info("批量重启模拟器 : {}, 成功", simulatorDto.getSimulatorName());
            } catch (Exception e) {
                LOGGER.error("批量重启模拟器 : {}, 失败", simulatorDto.getSimulatorName(), e);
            }
        }
        return HttpResponse.ok();
    }

    /**
     * 关闭模拟器
     *
     * @param simulatorId
     * @throws Exception
     */
    @RequestMapping(value = "/shutdown/{simulatorId}", method = RequestMethod.POST)
    @ResponseBody
    public Result shutdown(@PathVariable("simulatorId") String simulatorId) {
        SimulatorDto simulatorDto = this.dubboSimulatorRemoteService.getSimulatorDtoInfo(simulatorId);
        this.dubboSimulatorRemoteService.refreshSimulatorPowerState(simulatorDto.getId(), PowerState.CLOSE.ordinal());
        try {
            this.simulatorService.shutdownSimulator(simulatorDto);
            LOGGER.info("关闭模拟器 : {}, 成功", simulatorDto.getSimulatorName());
            return HttpResponse.ok();
        } catch (Exception e) {
            LOGGER.error("关闭模拟器 : {}, 失败", simulatorDto.getSimulatorName());
            return HttpResponse.error(e);
        }
    }

    /**
     * 关闭模拟器
     *
     * @throws Exception
     */
    @RequestMapping(value = "/batch_shutdown", method = RequestMethod.POST)
    @ResponseBody
    public Result shutdown() {
        for (SimulatorDto simulatorDto : dubboSimulatorRemoteService.getSimulatorDtoList()) {
            this.dubboSimulatorRemoteService.refreshSimulatorPowerState(simulatorDto.getId(), PowerState.CLOSE.ordinal());
            try {
                this.simulatorService.shutdownSimulator(simulatorDto);
                LOGGER.info("批量关闭模拟器 : {}, 成功", simulatorDto.getSimulatorName());
            } catch (Exception e) {
                LOGGER.error("批量关闭模拟器 : {}, 失败", simulatorDto.getSimulatorName());
            }
        }
        return HttpResponse.ok();
    }

    /**
     * 销毁模拟器
     *
     * @param simulatorId
     * @throws Exception
     */
    @RequestMapping(value = "/destroy/{simulatorId}", method = RequestMethod.POST)
    @ResponseBody
    public Result destroy(@PathVariable("simulatorId") String simulatorId) {
        SimulatorDto simulatorDto = this.dubboSimulatorRemoteService.getSimulatorDtoInfo(simulatorId);
        this.dubboSimulatorRemoteService.removeSimulatorInfo(simulatorId);
        try {
            this.simulatorService.destroySimulator(simulatorDto);
            LOGGER.info("销毁模拟器 : {}, 成功", simulatorDto.getSimulatorName());
            return HttpResponse.ok();
        } catch (Exception e) {
            LOGGER.error("销毁模拟器 : {}, 失败", simulatorDto.getSimulatorName(), e);
            return HttpResponse.error(e);
        }
    }

    /**
     * 创建模拟器
     *
     * @param httpServletRequest
     * @throws Exception
     */
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @ResponseBody
    public Result create(HttpServletRequest httpServletRequest) {
        String simulatorName = httpServletRequest.getParameter("simulatorName");
        String instance = httpServletRequest.getParameter("instance");
        try {
            SimulatorDto simulatorDto = this.dubboSimulatorRemoteService.createSimulatorInfo(simulatorName, instance);
            this.simulatorService.createSimulator(simulatorDto);
            LOGGER.info("创建模拟器 : {}, 成功", simulatorName);
            return HttpResponse.ok();
        } catch (Exception e) {
            LOGGER.error("创建模拟器 : {}, 失败", simulatorName);
            return HttpResponse.error(e);
        }
    }
}
