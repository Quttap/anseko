package org.i9.slb.platform.anseko.console.utils;

import org.i9.slb.platform.anseko.common.BusinessException;
import org.i9.slb.platform.anseko.common.ErrorCode;
import org.i9.slb.platform.anseko.common.VirtualEnum;
import org.i9.slb.platform.anseko.hypervisors.AbstractSimulatorBuilder;
import org.i9.slb.platform.anseko.hypervisors.AbstractSimulatorOperate;
import org.i9.slb.platform.anseko.hypervisors.kvm.KVMSimulatorBuilder;
import org.i9.slb.platform.anseko.hypervisors.kvm.KVMSimulatorOperate;
import org.i9.slb.platform.anseko.hypervisors.param.SimulatorInfo;

/**
 * 构建器工厂
 *
 * @author R12
 * @version 1.0
 * @date 2018/9/6 16:28
 */
public class SimulatorBuilderFactory {

    /**
     * 生成模拟器构建器
     *
     * @param simulatorInfo
     * @param virtualEnum
     * @return
     */
    public static AbstractSimulatorBuilder make(SimulatorInfo simulatorInfo, VirtualEnum virtualEnum) {
        if (virtualEnum == VirtualEnum.KVM) {
            return new KVMSimulatorBuilder(simulatorInfo);
        }
        throw new BusinessException(ErrorCode.MAKE_BUILDER_ERROR, "模拟构建器生成失败");
    }

    /**
     * 生成模拟器操作器
     *
     * @param simulatorName
     * @param virtualEnum
     * @return
     */
    public static AbstractSimulatorOperate make(String simulatorName, VirtualEnum virtualEnum) {
        if (virtualEnum == VirtualEnum.KVM) {
            return new KVMSimulatorOperate(simulatorName);
        }
        throw new BusinessException(ErrorCode.MAKE_BUILDER_ERROR, "模拟构建器生成失败");
    }
}
