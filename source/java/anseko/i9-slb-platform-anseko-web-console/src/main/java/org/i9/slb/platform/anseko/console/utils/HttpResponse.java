package org.i9.slb.platform.anseko.console.utils;

import org.i9.slb.platform.anseko.common.BusinessException;
import org.i9.slb.platform.anseko.common.ErrorCode;

/**
 * http响应数据封装
 *
 * @author R12
 * @version 1.0
 * @date 2018/8/30 14:46
 */
public class HttpResponse {

    public static Result ok() {
        Result result = new Result();
        result.setResult(ErrorCode.SUCCESS);
        result.setMessage("操作成功");
        return result;
    }

    public static Result error(BusinessException e) {
        Result result = new Result();
        result.setResult(e.getResult());
        result.setMessage(e.getMessage());
        return result;
    }

    public static Result error(Exception e) {
        Result result = new Result();
        if (e instanceof BusinessException) {
            BusinessException businessException = (BusinessException) e;
            result.setResult(businessException.getResult());
            result.setMessage(businessException.getMessage());
        } else {
            result.setResult(ErrorCode.UNKOWN_ERROR);
            result.setMessage("未知错误");
        }
        return result;
    }
}
