$("#start-btn").click(function () {
    if (!confirm("确认要启动当前模拟器吗？")) {
        return;
    }
    ajaxSimulator("start");
});
$("#shutdown-btn").click(function () {
    if (!confirm("确认要关闭当前模拟器吗？")) {
        return;
    }
    ajaxSimulator("shutdown");
});
$("#reboot-btn").click(function () {
    if (!confirm("确认要重启当前模拟器吗？")) {
        return;
    }
    ajaxSimulator("reboot");
});
$("#destroy-btn").click(function () {
    if (!confirm("确认要销毁当前模拟器吗？")) {
        return;
    }
    ajaxSimulator("destroy");

});
$("#focus-btn").click(function () {
    document.getElementById('vncview').focus();
    alert('获取用户输入焦点成功，你可以管理桌面并进行输入');
});
$("#load-btn").click(function () {
    init();
});
$(".close-btn").click(function () {
    var basePath = $("#basePath").val();
    window.opener.location.href = basePath + "/";
    window.close();
});

function ajaxSimulator(str) {
    var basePath = $("#basePath").val();
    var simulatorId = $("#simulatorId").val();
    $.post(basePath + "/" + str + "/" + simulatorId, function (data) {
        alert(data.message);
        if (str == 'destroy') {
            var basePath = $("#basePath").val();
            window.opener.location.href = basePath + "/";
            window.close();
            return;
        }
        else {
            init();
        }
    });
}

function init() {
    var simulatorId = $("#simulatorId").val();
    document.getElementById('vncview').src = 'http://61.181.128.231:20011/vncserver/vnc?simulatorId=' + simulatorId + '&rand=' + Math.random();
}

init();
