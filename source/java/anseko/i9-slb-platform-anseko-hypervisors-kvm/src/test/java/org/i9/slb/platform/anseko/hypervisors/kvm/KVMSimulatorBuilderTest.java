package org.i9.slb.platform.anseko.hypervisors.kvm;

import org.i9.slb.platform.anseko.hypervisors.AbstractSimulatorBuilder;
import org.i9.slb.platform.anseko.hypervisors.param.SimulatorDisk;
import org.i9.slb.platform.anseko.hypervisors.param.SimulatorInfo;
import org.i9.slb.platform.anseko.hypervisors.param.SimulatorNetwork;
import org.i9.slb.platform.anseko.hypervisors.param.SimulatorViewer;
import org.junit.Test;

import java.util.UUID;

/**
 * kvm虚拟化构建器
 *
 * @author R12
 * @version 1.0
 * @date 2018/8/30 13:27
 */
public class KVMSimulatorBuilderTest {

    @Test
    public void testMakeDefineSimulatorFile() {
        SimulatorInfo simulatorInfo = new SimulatorInfo("android.004.idc9000.com", 4, 4096);

        SimulatorViewer simulatorViewer = new SimulatorViewer(5966, "123456");
        simulatorInfo.setSimulatorViewer(simulatorViewer);

        SimulatorDisk simulatorDisk = new SimulatorDisk(UUID.randomUUID().toString(), "/dev/disk1/android.002.idc9000.com");
        simulatorInfo.getSimulatorDisks().add(simulatorDisk);

        SimulatorNetwork simulatorNetwork = new SimulatorNetwork(
                UUID.randomUUID().toString(), "vnet-" + simulatorInfo.getUuid(), "52:54:00:e7:dd:08", 10);
        simulatorInfo.getSimulatorNetworks().add(simulatorNetwork);

        AbstractSimulatorBuilder simulatorBuilder = new KVMSimulatorBuilder(simulatorInfo);
        String xml = simulatorBuilder.makeDefineSimulatorFile();
        System.out.println(xml);
    }
}