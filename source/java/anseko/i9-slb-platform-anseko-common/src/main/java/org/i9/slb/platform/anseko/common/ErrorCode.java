package org.i9.slb.platform.anseko.common;

/**
 * 返回错误码常量类
 *
 * @author R12
 * @date 2018.08.28
 */
public class ErrorCode {

    /**
     * 执行成功
     */
    public static final Integer SUCCESS = 0;

    /**
     * 未知错误
     */
    public static final Integer UNKOWN_ERROR = 1;

    /**
     * 命令执行失败
     */
    public static final Integer SHELL_PROCESS_ERROR = 2;

    /**
     * 未知命令
     */
    public static final Integer UNKNOWN_COMMAND_ERROR = 3;

    /**
     * shell 命令执行失败
     */
    public static final Integer SHELL_REMOTE_COMMAND_EXEC_ERROR = 4;

    /**
     * 生成构建器失败
     */
    public static final int MAKE_BUILDER_ERROR = 5;


}
