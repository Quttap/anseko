package org.i9.slb.platform.anseko.common.dubbo;

import org.i9.slb.platform.anseko.common.ErrorCode;

/**
 * dubbo返回结果
 *
 * @param <T>
 * @author R12
 * @date 2018.08.28
 */
public class DubboResult<T> implements java.io.Serializable {

    public DubboResult() {
        this.code = ErrorCode.SUCCESS;
        this.msg = "";
    }

    /**
     * 错误码
     */
    private Integer code;

    /**
     * 错误消息
     */
    private String msg;

    /**
     * 返回结果
     */
    private T re;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getRe() {
        return re;
    }

    public void setRe(T re) {
        this.re = re;
    }
}
