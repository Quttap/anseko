package org.i9.slb.platform.anseko.downstream.dto.param;

/**
 * @author R12
 * @version 1.0
 * @date 2018/8/30 18:09
 */
public class CommandParamDto implements java.io.Serializable {

    /**
     * 命令编号
     */
    private String commandId;

    /**
     * 执行命令行
     */
    private String commandLine;

    public String getCommandId() {
        return commandId;
    }

    public void setCommandId(String commandId) {
        this.commandId = commandId;
    }

    public String getCommandLine() {
        return commandLine;
    }

    public void setCommandLine(String commandLine) {
        this.commandLine = commandLine;
    }
}
