package org.i9.slb.platform.anseko.vncserver.servlet;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class VNCServlet extends HttpServlet {

    private static final long serialVersionUID = -2165483068708401298L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException {
        handleRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException {
        handleRequest(request, response);
    }

    protected void handleRequest(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession(true);
        if (session != null) {
            session.removeAttribute("keyValuesDto");
            session.setAttribute("simulatorId", request.getParameter("simulatorId"));
            session.setAttribute("chk", "true");
        }
        try {
            dispatch("/", request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void dispatch(String pageUri, HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        RequestDispatcher dispatch = getServletContext().getRequestDispatcher(pageUri);
        dispatch.forward(req, res);
    }
}
